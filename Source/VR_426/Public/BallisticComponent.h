// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BulletsDataAsset.h"
#include "BallisticComponent.generated.h"

class UMaterialsDataAsset;
class UBullet;
class UWindDirectionalSourceComponent;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UBallisticComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBallisticComponent();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	UMaterialsDataAsset* MaterialsDataAsset;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	bool AllowWind;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	float OverrideGravityZ;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	float MaxSimTime = 2.f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	UParticleSystem* PathTrail;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	FName PathTrailTargetName;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Simple Ballistic | Settings")
	bool bDebug;

	protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UWindDirectionalSourceComponent* WindComponent;

	public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, meta = (DisplayName = "Make Projectile", Keywords = "Projectile Simple Ballistic"), Category = "Simple Ballistic")
    UBullet* MakeProjectile(FProjectileData data, FVector startLocation, FVector direction, float dispersion, TArray<AActor*> IgnoredActors);
		
		
};
