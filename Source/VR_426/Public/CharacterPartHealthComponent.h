// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CharacterHealthComponent.h"
#include "CharacterPartHealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDetachPart, FName, BoneName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEmptyPartHealth, FName, BoneName);

UCLASS(Blueprintable, BlueprintType)
class VR_426_API UCharacterPartHealthComponent : public UCharacterHealthComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="HealthComponent|Character Part")
		FDetachPart OnDetachPart;

	UPROPERTY(BlueprintAssignable, Category="HealthComponent|Character Part")
		FEmptyPartHealth OnEmptyPartHealth;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		FName BoneName;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		bool bMultyMeshCharacter;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		FName ParticleSocketName;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		TSubclassOf<AActor> SpawningPartClass;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		float DetachDistance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		float DamageModifier;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		UCurveFloat* PhysicsBlendCurve;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Character Part")
		bool bCanDetach;

	UPROPERTY()
		bool bDetached;
	
	UPROPERTY()
		AActor* SpawnedPart;

	UPROPERTY()
		FPoseSnapshot Pose;

	UPROPERTY()
		UPrimitiveComponent* HitComponent;

	UPROPERTY()
		FVector HitLocation;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	virtual void _applyDamage(float Damage, UPrimitiveComponent* HitComp, FVector HitLoc) override;

	virtual void SetCurrentHP(float Value) override;

	bool CanDetach();

	void HideDetachedBones();

	USkeletalMeshComponent* FindChildMeshByTag(UPrimitiveComponent* ParentComponent, FName Tag);

	USkeletalMeshComponent* GetCharacterMesh();

	FName GetPrevBoneName(USkeletalMeshComponent* Mesh, FName Bone);
	
	FName GetNextBoneName(USkeletalMeshComponent* Mesh, FName Bone);

	float GetPhysicsBlendWeight();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="HealthComponent|Character Part")
		void DetachPart();


	
};
