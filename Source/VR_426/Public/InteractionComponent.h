// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractionComponentAbstract.h"
#include "InteractionComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class VR_426_API UInteractionComponent : public UInteractionComponentAbstract
{
	GENERATED_BODY()

public:

    UPROPERTY(BlueprintAssignable, Category="InteractionComponent")
        FFocused OnFocused;

	UPROPERTY(BlueprintAssignable, Category="InteractionComponent")
        FFocusLoct OnFocusLost;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="InteractionComponent")
        float TraceLenght;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="InteractionComponent")
        float TraceRadius;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="InteractionComponent")
    TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;

	UPROPERTY(BlueprintReadOnly, Category="InteractionComponent")
		AActor* FocusedItem;
	
protected:

    UCameraComponent* Camera;

	FHitResult Hit;

	TArray<AActor*> IgnoredActors;

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="InteractionComponent")
        void InteractEvent();

	virtual void InitializeComponent() override;

protected:

	virtual void Focused() override;

	virtual void FocusLost() override;

};
