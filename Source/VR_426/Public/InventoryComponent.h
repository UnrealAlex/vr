// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ContainerObject.h"
#include "InventoryComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAddItem, UObject*, ContainerObject);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemoveItem, UObject*, ContainerObject);

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="InventoryComponent")
		FAddItem OnAddItem;

	UPROPERTY(BlueprintAssignable, Category="InventoryComponent")
		FRemoveItem OnRemoveItem;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="InventoryComponent")
		TArray<UContainerObject*> Items;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="InventoryComponent")
		int MaxItemsCount;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="InventoryComponent")
		TArray<TSubclassOf<UContainerObject>> AllowObjectTypes; 

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="InventoryComponent")
		FVector DropLocationOffset;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UFUNCTION(BlueprintCallable)
		bool AddItem(UContainerObject* item);

	UFUNCTION(BlueprintCallable)
		bool AddStackableItem(UContainerObject* item);
	
	UFUNCTION(BlueprintCallable)
        bool RemoveItem(UContainerObject* item);

	UFUNCTION(BlueprintCallable)
        bool DropItem(UContainerObject* item);

	UFUNCTION()
		void OnItemDestroyed(UObject* Object);

	bool CanAddItem(UContainerObject* Item);

	bool CanAddItemByClass(TSubclassOf<UContainerObject> ItemClass);
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Sets default values for this component's properties
	UInventoryComponent();

	

	
};
