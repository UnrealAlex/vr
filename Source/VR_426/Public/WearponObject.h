// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ContainerObject.h"
#include "WearponObject.generated.h"

class UClip;

UCLASS()
class VR_426_API UWearponObject : public UContainerObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, Category="WearponObject")
		UClip* Clip;

	UFUNCTION(BlueprintCallable, Category="WearponObject")
	void AddClip(int ClipSize, int BulletCount);

	void GetClipParams(int& ClipSize, int& BulletCount);
	
};
