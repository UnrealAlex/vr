// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AmmoObject.h"
#include "InventoryComponent.h"
#include "Components/ActorComponent.h"
#include "WearponManagerComponent.generated.h"

class UContainerObject;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAddWearpon, UContainerObject*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRemoveWearpon, UContainerObject*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FChangeCurrentWearpon, int, CurrentWearponId);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEquipWearpon, AActor*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUnequipWearpon, AActor*, Item);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FReloadWearpon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUnload, UAmmoObject*, Ammo);

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UWearponManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FAddWearpon OnAddWearpon;

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FRemoveWearpon OnRemoveWearpon;

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FUnequipWearpon OnEquipWearpon;

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FUnequipWearpon OnUnequipWearpon;

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FChangeCurrentWearpon OnChangeCurrentWearpon;

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FReloadWearpon OnReloadWearpon;

	UPROPERTY(BlueprintAssignable, Category="WearponManagerComponent")
		FUnload OnUnloadWearpon;

	UPROPERTY(BlueprintReadOnly, Category="WearponManagerComponent")
		TArray<UContainerObject*> WearponSlots;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponManagerComponent")
		int WearponSlotsCount;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponManagerComponent")
		FName CharacterMeshTag;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponManagerComponent")
		FName WearponSocketName;

	UPROPERTY(BlueprintReadOnly, Category="WearponManagerComponent")
		AActor* CurrentWearpon;

	UPROPERTY(BlueprintReadOnly, Category="WearponManagerComponent")
		int CurrentWearponId;

	UPROPERTY(BlueprintReadOnly, Category="WearponManagerComponent")
		bool bWearponEquipped;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponManagerComponent")
		float AttachDelay;

	FTimerHandle AttachTimer;

	FTimerHandle DetachTimer;

	UPROPERTY(BlueprintReadOnly, Category="WearponManagerComponent")
		USkeletalMeshComponent* CharacterMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponManagerComponent")
		FName WearponComtainerTag;
	
	UPROPERTY(BlueprintReadOnly, Category="WearponManagerComponent")
		UInventoryComponent* WearponContainer;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	// Sets default values for this component's properties
	UWearponManagerComponent();
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        bool AttachItemToSocket(AActor* Item, UMeshComponent* ParentComponent, FName SocketName);

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        bool DetachItem(AActor* Item);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponManagerComponent")
        void EquipWearpon(UContainerObject* Item);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponManagerComponent")
        void UnequipWearpon();

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        bool CanAdd();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponManagerComponent")
        bool Add(AActor* Item);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponManagerComponent")
        bool Delete(UContainerObject* Item);

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
		void SetCurrentNext();

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        void Reload();

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        void SetCurrentPrew();

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        void ChangeWearpon(int WearponId);

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
		void DropCurrentWearpon();

	UFUNCTION(BlueprintCallable, Category="WearponManagerComponent")
        float GetMontageTime(UAnimMontage* Montage);

	bool CanChangeWearpon();
	
};
