// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ItemObject.h"
#include "Components/ActorComponent.h"
#include "ItemComponent.generated.h"

class UContainerObject;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UItemComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="ItemComponent")
		float Weight;
		
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="ItemComponent")
        TSubclassOf<UContainerObject> ContainerObjectClass;

	bool IsDestroyed;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
public:

	// Sets default values for this component's properties
	UItemComponent();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="ItemComponent")
		UContainerObject* CreateContainerObject();
	
	virtual UContainerObject* CreateContainerObj();

	void DestroyItem();
};
