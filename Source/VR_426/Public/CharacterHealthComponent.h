// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HealthComponent.h"
#include "CharacterHealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBleeding, float, BleedingIntencity);

UCLASS(Blueprintable, BlueprintType)
class VR_426_API UCharacterHealthComponent : public UHealthComponent
{
	GENERATED_BODY()

public:

	UCharacterHealthComponent();

	UPROPERTY(BlueprintAssignable, Category="HealthComponent|Bleeding")
		FBleeding OnBleeding;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Bleeding")
		float BleedingIntensity;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(ClampMin="0", ClampMax="1"), Category="HealthComponent|Bleeding")
		float BleedingChance;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Bleeding")
		float BleedingPeriod;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthComponent|Bleeding")
		bool bCanBleeding;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(ClampMin="0", ClampMax="1"), Category="HealthComponent|Bleeding")
		float BleedingStrength;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, meta=(ClampMin="0", ClampMax="1"), Category="HealthComponent|Bleeding")
		float BleedingStrengthDecrease;

	UPROPERTY()
		FTimerHandle BleedingTimerHandle;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	virtual void _applyDamage(float Damage, UPrimitiveComponent* HitComponent, FVector Location) override;

	UFUNCTION(BlueprintNativeEvent, Category="HealthComponent|Bleeding")
		bool CanBleeding();

	UFUNCTION(BlueprintCallable, Category="HealthComponent|Bleeding")
		void StartBleeding();
		
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="HealthComponent|Bleeding")
        void Bleeding(float BleedingIntencity);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="HealthComponent|Bleeding")
        void StopBleeding();
};
