// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "MaterialsDataAsset.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FMaterialData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	UPhysicalMaterial* PhysicalMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	float StrengthCoefficient;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	bool bCanRicochet;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	float DecelerationCoefficient = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	float DispersionCoefficient = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(ClampMin="0", ClampMax="1"), Category = "Simple Ballistic | Material Settings")
	float RicochetDecelerationCoefficient = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	float RicochetDispersionCoefficient = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	float RicochetMaxAngle = 30.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	UParticleSystem* HitParticle;

	bool IsValid()
	{
		if (PhysicalMaterial)
		{
			return true;
		}

		return false;
	}
};

class UPhysicalMaterial;

UCLASS(Blueprintable, BlueprintType)
class VR_426_API UMaterialsDataAsset : public UDataAsset
{
	GENERATED_BODY()

	public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Material Settings")
	TArray<FMaterialData> MaterialTypes;

	UFUNCTION(BlueprintCallable, Category = "Simple Ballistic | Material Settings")
        FMaterialData GetDataByMaterial(UPhysicalMaterial* material);
	
};
