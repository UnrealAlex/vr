// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/ActorComponent.h"
#include "InteractionComponentAbstract.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFocused, AActor*, FocusedActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FFocusLoct);

UCLASS(Abstract, BlueprintType)
class VR_426_API UInteractionComponentAbstract : public UActorComponent
{
	GENERATED_BODY()



public:
	
    UInteractionComponentAbstract();
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;	

	virtual void InitializeComponent() override;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	virtual void Focused();

	virtual void FocusLost();

	

	

	


		
};
