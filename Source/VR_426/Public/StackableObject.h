// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ContainerObject.h"
#include "StackableObject.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FStackCountChanged, UContainerObject*, ContainerObject, int, ChangeValue);

UCLASS()
class VR_426_API UStackableObject : public UContainerObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="StackableObject")
		FStackCountChanged OnStackChanged;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="StackableObject")
		int StackSize;

	UPROPERTY(BlueprintReadOnly, Category="StackableObject")
        int ItemsCount;

	UFUNCTION(BlueprintCallable, Category="StackableObject")
		int GetItems(int getCount);
		
	UFUNCTION(BlueprintCallable, Category="StackableObject")
		int GetAllItems();

	UFUNCTION(BlueprintCallable, Category="StackableObject")
        int AddItems(int addCount);

	virtual AActor* Drop(FVector Location, FRotator Rotation) override;
};
