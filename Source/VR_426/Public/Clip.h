// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Clip.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class VR_426_API UClip : public UObject
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="Clip")
		int ClipSize;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="Clip")
		int BulletCount;
};
