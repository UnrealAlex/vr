// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InteractionManagerComponent.generated.h"

class UContainerObject;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UInteractionManagerComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Sets default values for this component's properties
	UInteractionManagerComponent();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="InteractionManager")
		void ManageItem(AActor* Item);

	UFUNCTION(BlueprintCallable, Category="InteractionManager")
        UContainerObject* CreateContainerObject(AActor* Item);

	
};
