// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "ContainerObject.h"
#include "Components/ActorComponent.h"
#include "InventoryManagerComponent.generated.h"

class UInventoryComponent;

UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UInventoryManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")
		TArray<UInventoryComponent*> GetInventorys();
		
	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")		
		UInventoryComponent* GetInventoryByAllowObjectClasses(TSubclassOf<UContainerObject> ObjectClass);

	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")
		TArray<UContainerObject*> GetObjectsByClass(TSubclassOf<UContainerObject> ObjectClass);
	
	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")
		UContainerObject* AddItem(AActor* Item);

	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")
        UContainerObject* AddObject(UContainerObject* Obj);

	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")
        bool RemoveObject(UContainerObject* Obj);

	UFUNCTION(BlueprintCallable, Category="InventoryManagerComponent")
		void DropItem(UContainerObject* item);
};
