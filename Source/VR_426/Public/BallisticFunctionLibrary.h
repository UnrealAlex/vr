// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/GameplayStaticsTypes.h"
#include "BulletsDataAsset.h"
#include "BallisticFunctionLibrary.generated.h"

class UBullet;
/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class VR_426_API UBallisticFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static void SimplePredictProjectilePath(const UObject* WorldContextObject, FProjectileData ProjectileData, UWindDirectionalSourceComponent* WindComponent, const FPredictProjectilePathParams& PredictParams, FPredictProjectilePathResult& PredictResult);
};
