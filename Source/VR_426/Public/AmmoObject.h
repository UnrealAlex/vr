// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "StackableObject.h"
#include "AmmoObject.generated.h"

/**
 * 
 */
UCLASS()
class VR_426_API UAmmoObject : public UStackableObject
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="AmmoObject")
		FString AmmoName;
	
};
