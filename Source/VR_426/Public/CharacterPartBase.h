// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "CharacterPartBase.generated.h"

UCLASS()
class VR_426_API ACharacterPartBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACharacterPartBase();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="CharacterPart")
		USkeletalMeshComponent* PartMesh;

	UPROPERTY(BlueprintReadOnly, Category="CharacterPart")
		FPoseSnapshot Pose;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
