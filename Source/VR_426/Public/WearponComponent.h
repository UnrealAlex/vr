// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "AmmoObject.h"
#include "BulletsDataAsset.h"
#include "ItemComponent.h"
#include "WearponComponent.generated.h"

class UBulletsDataAsset;

UCLASS()
class VR_426_API UWearponComponent : public UItemComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent")
		int ClipSize;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="WearponComponent")
		float Dispersion;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent")
		float ShotsPerSecond;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent|Bullets")
		UBulletsDataAsset* BulletsDataAsset;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent|Bullets")
		FVector AimOffset;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="WearponComponent|Bullets")
        TArray<FString> BulletTypes;

	UPROPERTY(BlueprintReadWrite, Category="WearponComponent|Bullets")
        int CurrentBulletTypeId;

	UPROPERTY(BlueprintReadWrite, Category="WearponComponent|Bullets")
		UAmmoObject* CurrentBulletObj;

	UPROPERTY(BlueprintReadWrite, Category="WearponComponent")
		USkeletalMeshComponent* CharacterMesh;

	UPROPERTY(BlueprintReadWrite, Category="WearponComponent")
		int BulletCount;

	UPROPERTY(BlueprintReadWrite, Category="WearponComponent")
		bool bReloading;

	UPROPERTY(BlueprintReadWrite, Category="WearponComponent")
		bool bCanFire;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent")
		UAnimMontage* ReloadMontage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent")
		UAnimMontage* EquipMontage;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="WearponComponent")
		UAnimMontage* UnequipMontage;
		
	
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	UWearponComponent();
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponComponent")
		void Reload(UAmmoObject* ammo);

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponComponent")
        UAmmoObject* Unload();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponComponent")
        bool CanShot();

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="WearponComponent")
        void DecrementBullets();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="WearponComponent")
        FString GetCurrentBulletType();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category="WearponComponent")
        FProjectileData GetCurrentAmmoData();



	void SetCurrentBulletTypeID(int TypeId);
};
