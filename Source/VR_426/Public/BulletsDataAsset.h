// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BulletsDataAsset.generated.h"

USTRUCT(BlueprintType, Blueprintable)
struct FProjectileData
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SimpleBallistic|Projectile Settings")
	FString Name;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "(mm)"), Category="SimpleBallistic|Projectile Settings")
	float Caliber;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "(g)"), Category = "Simple Ballistic | Projectile Settings")
	float Weight;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "(m/s)"), Category = "Simple Ballistic | Projectile Settings")
	float StartSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Projectile Settings")
	float BallisticCoefficient = 0.2f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Projectile Settings")
	float ArmorPiercingCoefficient = 1.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Projectile Settings")
	bool bMultyProjectile = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta=(EditCondition = "bMultyProjectile"), Category = "Simple Ballistic | Projectile Settings")
	int ProjectileCount = 4;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Projectile Settings")
	TEnumAsByte<ECollisionChannel> CollisionChannel = ECollisionChannel::ECC_Camera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Simple Ballistic | Projectile Settings")
	TArray<TEnumAsByte<ECollisionChannel>> ObjectTypes;
	
	bool IsValid()
	{
		if (!Name.IsEmpty())
		{
			return true;
		}

		return false;
	}
};

UCLASS(Blueprintable, BlueprintType)
class VR_426_API UBulletsDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SimpleBallistic|Projectile Settings")
	TArray<FProjectileData> ProjectileTypes;

	UFUNCTION(BlueprintCallable, Category = "Simple Ballistic | Projectile Settings")
        FProjectileData GetDataByName(FString name);
	
};
