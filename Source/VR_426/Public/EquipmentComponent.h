// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "EquipmentComponent.generated.h"

class UContainerObject;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEquip, UContainerObject*, Item);

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UEquipmentComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="EquipmentComponent")
		FEquip OnEquip;

	UPROPERTY(BlueprintReadOnly, Category="EquipmentComponent")
		TArray<UContainerObject*> Equipment;

	UPROPERTY(BlueprintReadOnly, Category="EquipmentComponent")
		int EquipmentSize;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Sets default values for this component's properties
    UEquipmentComponent();

	UFUNCTION(BlueprintCallable, Category="EquipmentComponent")
		bool CanEquip();

	UFUNCTION(BlueprintCallable, Category="EquipmentComponent")
        bool Equip(UContainerObject* Item);

	UFUNCTION(BlueprintCallable, Category="EquipmentComponent")
        void Unequip(UContainerObject* Item);
};
