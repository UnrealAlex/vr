// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemComponent.h"
#include "StackableItemComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDestroyItem);

UCLASS(Blueprintable, BlueprintType)
class VR_426_API UStackableItemComponent : public UItemComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="StackablrItemComponent")
		FDestroyItem OnDestroyItem;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="StackablrItemComponent")
		int ItemsCount;

	UFUNCTION(BlueprintCallable, Category="StackablrItemComponent")
		int GetItems(int GetCount);

	UFUNCTION(BlueprintCallable, Category="StackablrItemComponent")
		int AddItems(int AddCount);

	virtual UContainerObject* CreateContainerObj() override;

	
	
};
