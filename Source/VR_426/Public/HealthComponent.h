// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEmptyHealth);

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable, Category="HealthComponent")
		FEmptyHealth OnEmptyHealth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="HealthComponent")
		float MaxHealth;

	UPROPERTY(BlueprintReadOnly, Category="HealthComponent")
		float CurrentHealth;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UHealthComponent();
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category="HealthComponent")
	void IncreaseHealth(float Value);

	UFUNCTION(BlueprintCallable, Category="HealthComponent")
    void DecreaseHealth(float Value);

	UFUNCTION(BlueprintNativeEvent, Category="HealthComponent")
	void ApplyDamage(float Damage, UPrimitiveComponent* HitComponent, FVector Location);

	virtual void _applyDamage(float Damage, UPrimitiveComponent* HitComponent, FVector Location);

	virtual void SetCurrentHP(float Value);
};
