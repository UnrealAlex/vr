// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "BulletsDataAsset.h"
#include "Kismet/GameplayStaticsTypes.h"
#include "MaterialsDataAsset.h"
#include "Bullet.generated.h"

class UBallisticComponent;


USTRUCT(BlueprintType, Blueprintable)
struct FProjectileHitResult
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "Speed at the moment of hitting an obstacle (m/s)"), Category="Simple Ballistic | ProjectileHitresult")
	float Speed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "Force at the moment of hitting an obstacle (J)"), Category = "Simple Ballistic | ProjectileHitresult")
	float Force;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "Distance traveled by a projectile (m)"), Category = "Simple Ballistic | ProjectileHitresult")
	float Distance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "Force needed to break through the barrier (J)"), Category = "Simple Ballistic | ProjectileHitresult")
	float BarrierBreakingForce;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "If true, the barrier was broken"), Category = "Simple Ballistic | ProjectileHitresult")
	bool IsBreaking;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "If true, the projectile rebounded"), Category = "Simple Ballistic | ProjectileHitresult")
	bool IsRicochet;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (ToolTip = "Physics material"), Category = "Simple Ballistic | ProjectileHitresult")
	FMaterialData MaterialData;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FProjectileHit, FVector, HitFromDirection, FProjectileData, projectileData, FProjectileHitResult, ProjectileHitResult, FHitResult, HitResult);


UCLASS(Blueprintable, BlueprintType)
class VR_426_API UBullet : public UObject
{
	GENERATED_BODY()

	private:

	UBullet();

	TArray<FPredictProjectilePathPointData> Points;
	FPredictProjectilePathParams ProjectileParams;
	FPredictProjectilePathResult result;
	TArray<FPredictProjectilePathResult> results;


	float deltaTime;
	float Speed;
	float Deceleration;
	float CurrentTime;
	float CurrentDistance;
	int TraceCounter;
	FTimerHandle Timer;
	FHitResult Hit;

	UBallisticComponent* BallisticComponent;
	UParticleSystemComponent* PathTrailComp;

	TArray<UBullet*> projectiles;

	void Destroy();

	public:

	UPROPERTY(BlueprintAssignable, Category="Simple Ballistic | Projectile Event")
	FProjectileHit OnProjectileHit;

	FProjectileData ProjectileData;
	FVector Direction;
	FVector StartLocation;
	float Dispersion = 0;
	float LaunchVelocity;
	float MaxSimTime;
	UMaterialsDataAsset* MaterialsData;
	bool bMultyProjectile;
	TArray<AActor*> IgnoredActors;

	UWindDirectionalSourceComponent* WindComponent;

	float BarrierBreakingForce;
	
	float DestroyTimer = 4.f;

	void Fire();

	void LineTrace();

	bool CheckBreaking(FHitResult HitResult, FVector direction, float angle, FMaterialData materialData, float& barrierThickness, FVector& breakLocation);
	bool CheckRicochet(FMaterialData materialData, float angle);
	void Break(FMaterialData materialData, float barrierThickness, FVector location);
	void Ricochet(FHitResult HitResult, FVector direction, float angle, FMaterialData materialData);

	UFUNCTION()
    void _ProjectileHit(FVector HitFromDirection, FProjectileData projectileData, FProjectileHitResult ProjectileHitResult, FHitResult HitResult);

	void SetBallisticComponent(UBallisticComponent* component);
	UBallisticComponent* GetBallisticComponent();

	virtual class UWorld* GetWorld() const override;
	
};
