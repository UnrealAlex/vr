// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "CharacterHealthComponent.h"
#include "CharacterPartHealthComponent.h"
#include "Components/ActorComponent.h"
#include "HealthManagerComponent.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_426_API UHealthManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthManagerComponent();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="HealthManager")
		FName MainHealthComponentTag;

	UPROPERTY(BlueprintReadOnly, Category="HealthManager")
		UCharacterHealthComponent* MainHealthComponent;

	UPROPERTY(BlueprintReadOnly, Category="HealthManager")
		TArray<UCharacterHealthComponent*> healthComponents;

	UPROPERTY(BlueprintReadOnly, Category="HealthManager")
		TArray<UCharacterPartHealthComponent*> partHealthComponents;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintNativeEvent, Category="HealthManager")
		void OnPartDetached(FName BoneName);

	UFUNCTION(BlueprintNativeEvent, Category="HealthManager")
        void OnBleeding(float BleedingIntencity);

	UFUNCTION(BlueprintNativeEvent, Category="HealthManager")
        void OnEmptyHealth();

	UFUNCTION(BlueprintNativeEvent, Category="HealthManager")
        void OnEmptyPartHealth(FName BoneName);

	UFUNCTION(BlueprintCallable, Category="HealthManager")
		void ApplyDamage(float Damage, FName BoneName, UPrimitiveComponent* HitComponent, FVector HitLocation);

	UCharacterPartHealthComponent* GetPartHealthByBoneName(FName BoneName);
};
