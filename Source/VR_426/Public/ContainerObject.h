// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ItemObject.h"
#include "UObject/NoExportTypes.h"
#include "ContainerObject.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDestroyObject, UObject*, Object);

UCLASS(Blueprintable, BlueprintType)
class VR_426_API UContainerObject : public UItemObject
{
	GENERATED_BODY()

public:	

	UPROPERTY(BlueprintAssignable)
		FDestroyObject OnDestroyObject;
	
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="ContainerObject")
		TSubclassOf<AActor> ItemClass;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category="ContainerObject")
		AActor* Owner;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category="ContainerObject")
		AActor* DropItem(FVector Location, FRotator Rotation);
	
	virtual AActor* Drop(FVector Location, FRotator Rotation);
	
	void Destroy();
};
