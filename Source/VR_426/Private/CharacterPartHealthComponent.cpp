// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPartHealthComponent.h"


#include "CharacterPartBase.h"
#include "Kismet/KismetMathLibrary.h"

void UCharacterPartHealthComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UCharacterPartHealthComponent::_applyDamage(float Damage, UPrimitiveComponent* HitComp, FVector HitLoc)
{
    if(!bDetached)
    {
        Super::_applyDamage(Damage, HitComponent, HitLoc);
    }

    HitComponent = HitComp;
    HitLocation = HitLoc;

    USkeletalMeshComponent* mesh = Cast<USkeletalMeshComponent>(HitComp);

    if(mesh)
    {
        mesh->SetAllBodiesBelowSimulatePhysics(BoneName, true, true);
        mesh->SetAllBodiesBelowPhysicsBlendWeight(BoneName, GetPhysicsBlendWeight(), false, true);
    }

    if(CanDetach())
    {
        DetachPart();        
    }
}

void UCharacterPartHealthComponent::SetCurrentHP(float Value)
{
    Super::SetCurrentHP(Value);

    if(CurrentHealth <= 0 && OnEmptyPartHealth.IsBound())
    {
        OnEmptyPartHealth.Broadcast(BoneName);
    }
}

bool UCharacterPartHealthComponent::CanDetach()
{
    if(!HitComponent)
    {
        return false;
    }

    float hitDistance = UKismetMathLibrary::Vector_Distance(HitComponent->GetSocketLocation(BoneName), HitLocation);
    
    if(CurrentHealth <= 0.f && bCanDetach && hitDistance < DetachDistance && !bDetached)
    {
        return true;
    }

    return false;
}

void UCharacterPartHealthComponent::HideDetachedBones()
{
    if(!HitComponent || !SpawnedPart)
    {
        return;
    }

    USkeletalMeshComponent* charMesh = GetCharacterMesh();

    if(!charMesh)
    {
        return;
    }

    FDetachmentTransformRules transformRules = FDetachmentTransformRules(EDetachmentRule::KeepRelative, true);

    USkeletalMeshComponent* partMesh = SpawnedPart->FindComponentByClass<USkeletalMeshComponent>();

    int startId = charMesh->GetBoneIndex(BoneName);
    int endId = charMesh->GetNumBones() - 1;

    TArray<USceneComponent*> partMeshChilds;

    partMesh->GetChildrenComponents(false, partMeshChilds);

    for(int i = startId; i <= endId; i++)
    {
        if(charMesh->IsBoneHidden(i))
        {
            FName boneName = charMesh->GetBoneName(i);
            
            if(bMultyMeshCharacter)
            {
                FName partBoneName = GetPrevBoneName(charMesh, boneName); 
                
                USkeletalMeshComponent* partChildMesh = FindChildMeshByTag(partMesh, partBoneName);
                if(partChildMesh)
                {
                    partChildMesh->HideBoneByName(partBoneName, EPhysBodyOp::PBO_Term);
                    partChildMesh->DetachFromComponent(transformRules);
                }
            }
            else
            {
                partMesh->HideBoneByName(boneName, EPhysBodyOp::PBO_Term);
            }
        }
    }

    charMesh->HideBoneByName(BoneName, EPhysBodyOp::PBO_Term);

    if(bMultyMeshCharacter)
    {
        charMesh->DetachFromComponent(transformRules);

        USkeletalMeshComponent* tmpCharMesh = Cast<USkeletalMeshComponent>(HitComponent);
        FName boneName = GetNextBoneName(tmpCharMesh, BoneName);

        if(tmpCharMesh->DoesSocketExist(boneName))
        {
            tmpCharMesh->HideBoneByName(boneName, EPhysBodyOp::PBO_Term);
        }
        else
        {
            tmpCharMesh->HideBoneByName(BoneName, EPhysBodyOp::PBO_Term);
        }        
        charMesh->SetWorldLocation(FVector(0.f, 0.f, -1000000.f));
    }
}

USkeletalMeshComponent* UCharacterPartHealthComponent::FindChildMeshByTag(UPrimitiveComponent* ParentComponent,
    FName Tag)
{
    USkeletalMeshComponent* resultMesh = nullptr;
    
    TArray<USceneComponent*> childComponents;
    Cast<USkeletalMeshComponent>(ParentComponent)->GetChildrenComponents(true, childComponents);

    USceneComponent** tmpComponentPtr = childComponents.FindByPredicate([&](USceneComponent* c)
    {
        return  c->ComponentHasTag(Tag);
    });

    if(tmpComponentPtr)
    {
        resultMesh = Cast<USkeletalMeshComponent>(*tmpComponentPtr);
    }

    return resultMesh;
}

USkeletalMeshComponent* UCharacterPartHealthComponent::GetCharacterMesh()
{
    USkeletalMeshComponent* charMesh = nullptr;
    
    if(bMultyMeshCharacter)
    {
        charMesh = FindChildMeshByTag(HitComponent, BoneName);
    }
    else
    {
        charMesh = Cast<USkeletalMeshComponent>(HitComponent);
    }

    return charMesh;
}

FName UCharacterPartHealthComponent::GetPrevBoneName(USkeletalMeshComponent* Mesh, FName Bone)
{
    int boneId = Mesh->GetBoneIndex(Bone) - 1;
    FName resultBoneName = Mesh->GetBoneName(boneId);

    return resultBoneName;
}

FName UCharacterPartHealthComponent::GetNextBoneName(USkeletalMeshComponent* Mesh, FName Bone)
{
    int boneId = Mesh->GetBoneIndex(Bone) + 1;
    FName resultBoneName = Mesh->GetBoneName(boneId);

    return resultBoneName;
}

float UCharacterPartHealthComponent::GetPhysicsBlendWeight()
{
    float tmpValue = 1 - CurrentHealth / MaxHealth;
    
    if(!PhysicsBlendCurve)
    {
        return  tmpValue;
    }

    return PhysicsBlendCurve->GetFloatValue(tmpValue);
}

void UCharacterPartHealthComponent::DetachPart_Implementation()
{
    FActorSpawnParameters params;
    params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    Cast<USkeletalMeshComponent>(HitComponent)->SnapshotPose(Pose);

    SpawnedPart = GetWorld()->SpawnActor<AActor>(SpawningPartClass, HitComponent->GetComponentLocation(), HitComponent->GetComponentRotation(), params);

    ACharacterPartBase* tmpPart = Cast<ACharacterPartBase>(SpawnedPart);
    
    if(tmpPart)
    {
        tmpPart->Pose = Pose;
    }

    FTimerHandle tmpTimer;

    GetWorld()->GetTimerManager().SetTimer(tmpTimer, [&]()
    {
        USkeletalMeshComponent* PartMesh = SpawnedPart->FindComponentByClass<USkeletalMeshComponent>();

        if(PartMesh)
        {
            PartMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
            PartMesh->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);
            PartMesh->SetSimulatePhysics(true);
        }
    }, 0.05f, false, 0.05f);

    HideDetachedBones();

    bDetached = true;

    if(OnDetachPart.IsBound())
    {
        OnDetachPart.Broadcast(BoneName);
    }
}
