// Fill out your copyright notice in the Description page of Project Settings.


#include "BallisticComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/WindDirectionalSource.h"
#include "Bullet.h"

// Sets default values for this component's properties
UBallisticComponent::UBallisticComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBallisticComponent::BeginPlay()
{
	Super::BeginPlay();

	AWindDirectionalSource* WindActor;
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetOwner(), AWindDirectionalSource::StaticClass(), FoundActors);

	if (FoundActors.Num() > 0)
	{
		WindActor = Cast<AWindDirectionalSource>(FoundActors[0]);
		WindComponent = WindActor->GetComponent();
	}
	
}


// Called every frame
void UBallisticComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UBullet* UBallisticComponent::MakeProjectile(FProjectileData data, FVector startLocation, FVector direction, float dispersion, TArray<AActor*> IgnoredActors)
{
	if (!data.IsValid())
	{
		return nullptr;
	}

	UBullet* projectile = NewObject<UBullet>(GetOwner());
	projectile->ProjectileData = data;
	projectile->Direction = direction;
	projectile->Dispersion = dispersion;
	projectile->StartLocation = startLocation;
	projectile->MaterialsData = MaterialsDataAsset;
	projectile->LaunchVelocity = data.StartSpeed;
	projectile->bMultyProjectile = data.bMultyProjectile;
	projectile->MaxSimTime = MaxSimTime;
	projectile->SetBallisticComponent(this);
	projectile->IgnoredActors.Append(IgnoredActors);

	if (AllowWind)
	{
		projectile->WindComponent = WindComponent;
	}

	FTimerHandle timer;
	
	GetWorld()->GetTimerManager().SetTimer(timer, projectile, &UBullet::Fire, 0.03, false);

	return projectile;
}