// Fill out your copyright notice in the Description page of Project Settings.


#include "WearponObject.h"
#include "Clip.h"

void UWearponObject::AddClip(int ClipSize, int BulletCount)
{
    if(!Clip)
    {
        Clip = NewObject<UClip>(Owner);
    }
    
    Clip->ClipSize = ClipSize;
    Clip->BulletCount = BulletCount;
}

void UWearponObject::GetClipParams(int& ClipSize, int& BulletCount)
{
    ClipSize = Clip->ClipSize;
    BulletCount = Clip->BulletCount;
}
