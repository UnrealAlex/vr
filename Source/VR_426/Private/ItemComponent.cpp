// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemComponent.h"
#include "ContainerObject.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UItemComponent::UItemComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	IsDestroyed = false;
	// ...
}


UContainerObject* UItemComponent::CreateContainerObj()
{
	auto containerObj = Cast<UContainerObject>(UGameplayStatics::SpawnObject(ContainerObjectClass, this));

	if(containerObj)
	{
		containerObj->ItemClass = GetOwner()->GetClass();
		containerObj->Owner = GetOwner();
	}

	return containerObj;
}

// Called when the game starts
void UItemComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UItemComponent::DestroyItem()
{
	FTimerHandle timer;

	if(!IsDestroyed)
	{	
		GetWorld()->GetTimerManager().SetTimer(timer, [&]()
		{
			GetOwner()->Destroy();
		}, 0.1f, false, 0.1f);
	}
	
	IsDestroyed = true;
}


// Called every frame
void UItemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UContainerObject* UItemComponent::CreateContainerObject_Implementation()
{
	return CreateContainerObj();
}

