// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterHealthComponent.h"

#include "Kismet/KismetMathLibrary.h"

UCharacterHealthComponent::UCharacterHealthComponent()
{
    BleedingStrength = 1.f;
}

void UCharacterHealthComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UCharacterHealthComponent::_applyDamage(float Damage, UPrimitiveComponent* HitComponent, FVector Location)
{
    Super::_applyDamage(Damage, HitComponent, Location);

    if(CanBleeding())
    {
        StartBleeding();
    }
}

bool UCharacterHealthComponent::CanBleeding_Implementation()
{
    float rnd = UKismetMathLibrary::RandomFloatInRange(0.f, 1.f);
    
    if(CurrentHealth > 0.f && bCanBleeding && rnd <= BleedingChance)
    {
        return true;
    }

    return false;
}

void UCharacterHealthComponent::StartBleeding()
{
    StopBleeding();

    GetWorld()->GetTimerManager().SetTimer(BleedingTimerHandle, [&]()
    {
        if(OnBleeding.IsBound())
        {
            if(BleedingStrength <= 0.f)
            {
                StopBleeding();
            }
            
            OnBleeding.Broadcast(MaxHealth * BleedingIntensity * BleedingStrength);
            Bleeding(MaxHealth * BleedingIntensity * BleedingStrength);
        }
    }, BleedingPeriod, true);
}

void UCharacterHealthComponent::Bleeding_Implementation(float BleedingIntencity)
{
    BleedingStrength -= BleedingStrengthDecrease;

    if(BleedingStrength <= 0.f)
    {
        BleedingStrength = 0.f;
    }
}

void UCharacterHealthComponent::StopBleeding_Implementation()
{
    GetWorld()->GetTimerManager().ClearTimer(BleedingTimerHandle);
    BleedingTimerHandle.Invalidate();
}
