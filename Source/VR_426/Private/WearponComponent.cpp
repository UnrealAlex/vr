// Fill out your copyright notice in the Description page of Project Settings.


#include "WearponComponent.h"

void UWearponComponent::Reload_Implementation(UAmmoObject* ammo)
{
    int bulletsCount = ammo->GetItems(ClipSize - BulletCount);

    if(bulletsCount > 0)
    {
        BulletCount += bulletsCount;
        bReloading = true;
        CurrentBulletObj = ammo;

        if(ReloadMontage)
        {
            FTimerHandle reloadTimer;
            GetWorld()->GetTimerManager().SetTimer(reloadTimer, [&]()
            {
                bReloading = false;
            }, ReloadMontage->CalculateSequenceLength(), false, ReloadMontage->CalculateSequenceLength());
        }
    }
}

bool UWearponComponent::CanShot_Implementation()
{
    if(BulletCount > 0 && !bReloading)
    {
        return true;
    }

    return false;
}

void UWearponComponent::DecrementBullets_Implementation()
{
    BulletCount -= 1;

    if(BulletCount < 0)
    {
        BulletCount = 0;
    }
}

void UWearponComponent::BeginPlay()
{
    Super::BeginPlay();
    FString name = GetReadableName();
    //ensureMsgf(BulletTypes.Num() > 0, TEXT("BulletTypes is Empty in %s"), *name);
}

UWearponComponent::UWearponComponent()
{
    Dispersion = 0.3f;
}

UAmmoObject* UWearponComponent::Unload_Implementation()
{
    if(CurrentBulletObj && BulletCount > 0)
    {
        CurrentBulletObj->ItemsCount = BulletCount;
        BulletCount = 0;

        return CurrentBulletObj;
    }

    return nullptr;
}

FString UWearponComponent::GetCurrentBulletType()
{
    if(BulletTypes.Num() > 0 && CurrentBulletTypeId < BulletTypes.Num())
    {
        return  BulletTypes[CurrentBulletTypeId];        
    }

    return FString();
}

FProjectileData UWearponComponent::GetCurrentAmmoData()
{
    return BulletsDataAsset->GetDataByName(GetCurrentBulletType());
}

void UWearponComponent::SetCurrentBulletTypeID(int TypeId)
{
    CurrentBulletTypeId = TypeId;
}

