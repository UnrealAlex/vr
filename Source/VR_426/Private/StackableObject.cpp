// Fill out your copyright notice in the Description page of Project Settings.


#include "StackableObject.h"

#include "StackableItemComponent.h"

int UStackableObject::GetItems(int getCount)
{
    if(getCount >= ItemsCount)
    {
        int tmpItemsCount = ItemsCount;
        ItemsCount = 0;

        OnStackChanged.Broadcast(this, -tmpItemsCount);
        
        Destroy();
        return tmpItemsCount;
    }

    ItemsCount -= getCount;

    OnStackChanged.Broadcast(this, -getCount);
    
    return getCount;
}

int UStackableObject::GetAllItems()
{
    return  GetItems(ItemsCount);
}

int UStackableObject::AddItems(int addCount)
{
    if(ItemsCount + addCount > StackSize)
    {
        int result = ItemsCount + addCount - StackSize;
        ItemsCount = StackSize;

        OnStackChanged.Broadcast(this, result);
        
        return result;
    }

    ItemsCount += addCount;

    OnStackChanged.Broadcast(this, addCount);
    
    return 0;
}

AActor* UStackableObject::Drop(FVector Location, FRotator Rotation)
{
    AActor* actor = Super::Drop(Location, Rotation);

    if(!actor)
    {
        return nullptr;
    }

    UStackableItemComponent* itemComp = actor->FindComponentByClass<UStackableItemComponent>();

    if(!itemComp)
    {
        return nullptr;
    }
    
    itemComp->ItemsCount = ItemsCount;

    return actor;
}
