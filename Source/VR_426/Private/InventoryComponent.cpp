// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryComponent.h"

#include "StackableObject.h"

// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	MaxItemsCount = 10;
	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

bool UInventoryComponent::AddItem(UContainerObject* item)
{
	if(!item)
	{
		return false;
	}

	if(!CanAddItem(item))
	{
		return false;
	}
	
	item->OnDestroyObject.AddUniqueDynamic(this, &UInventoryComponent::OnItemDestroyed);
	
	if(UStackableObject* stack = Cast<UStackableObject>(item))
	{
		TArray<UStackableObject*> stackableObjs;

		for(UContainerObject* o : Items)
		{
			if(UStackableObject* s = Cast<UStackableObject>(o))
			{
				if(s->ItemClass == stack->ItemClass)
				{
					stackableObjs.Add(s);
				}
			}
		}

		if(stackableObjs.Num() == 0)
		{
			Items.Add(item);
			OnAddItem.Broadcast(item);
			return false;
		}
		
		UStackableObject** invStackPtr = stackableObjs.FindByPredicate([stack](UStackableObject* s)
		{
			return s->ItemsCount < s->StackSize;
		});

		if(invStackPtr)
		{
			UStackableObject* invStack = Cast<UStackableObject>(*invStackPtr);

			int needCount = invStack->StackSize - invStack->ItemsCount;

			int addCount = stack->GetItems(needCount);

			invStack->AddItems(addCount);
			
			if(stack->ItemsCount > 0)
			{
				Items.Add(stack);
			}
		}
		else
		{
			Items.Add(item);
		}
	}
	else
	{
		Items.Add(item);
	}

	OnAddItem.Broadcast(item);
	
	return true;
}

bool UInventoryComponent::AddStackableItem(UContainerObject* item)
{
	return false;
}

bool UInventoryComponent::RemoveItem(UContainerObject* item)
{
	int removed = Items.Remove(item);

	if(removed > 0)
	{
		OnRemoveItem.Broadcast(item);
		
		item->OnDestroyObject.RemoveAll(this);
		return true;
	}

	return false;
}


//TODO relocate to inventory manager
bool UInventoryComponent::DropItem(UContainerObject* item)
{
	FVector Location = GetOwner()->GetActorLocation() +
		(GetOwner()->GetActorForwardVector() * DropLocationOffset.X) +
			(GetOwner()->GetActorRightVector() * DropLocationOffset.Y) +
				(GetOwner()->GetActorUpVector() * DropLocationOffset.Z);

	FRotator Rotation = GetOwner()->GetActorRotation();

	AActor* spawnedActor = item->DropItem(Location, Rotation);

	if(spawnedActor)
	{
		RemoveItem(item);
		
		return true;
	}

	return false;
}


void UInventoryComponent::OnItemDestroyed(UObject* Object)
{
	RemoveItem(Cast<UContainerObject>(Object));
}

bool UInventoryComponent::CanAddItem(UContainerObject* Item)
{
	if(Items.Num() < MaxItemsCount && (AllowObjectTypes.Num() == 0 || AllowObjectTypes.Contains(Item->GetClass())))
	{
		return true;
	}
	return false;
}

bool UInventoryComponent::CanAddItemByClass(TSubclassOf<UContainerObject> ItemClass)
{
	if(Items.Num() < MaxItemsCount && (AllowObjectTypes.Num() == 0 || AllowObjectTypes.Contains(ItemClass)))
	{
		return true;
	}
	return false;
}


// Called every frame
void UInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

