// Fill out your copyright notice in the Description page of Project Settings.


#include "EquipmentComponent.h"
#include "ContainerObject.h"

// Sets default values for this component's properties
UEquipmentComponent::UEquipmentComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	EquipmentSize = 2;
	// ...
}

bool UEquipmentComponent::CanEquip()
{
	return Equipment.Num() < EquipmentSize;
}

bool UEquipmentComponent::Equip(UContainerObject* Item)
{
	if(CanEquip())
	{
		Equipment.Add(Item);

		if(OnEquip.IsBound())
		{
			OnEquip.Broadcast(Item);
		}

		return true;
	}

	return false;
}

void UEquipmentComponent::Unequip(UContainerObject* Item)
{
	Equipment.Remove(Item);
}


// Called when the game starts
void UEquipmentComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UEquipmentComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

