// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthComponent.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	CurrentHealth = MaxHealth;
	
}


// Called every frame
void UHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHealthComponent::IncreaseHealth(float Value)
{
	SetCurrentHP(CurrentHealth + Value);
}

void UHealthComponent::DecreaseHealth(float Value)
{
	SetCurrentHP(CurrentHealth - Value);
}

void UHealthComponent::ApplyDamage_Implementation(float Damage, UPrimitiveComponent* HitComponent, FVector Location)
{
	_applyDamage(Damage, HitComponent, Location);
}

void UHealthComponent::_applyDamage(float Damage, UPrimitiveComponent* HitComponent, FVector Location)
{
	DecreaseHealth(Damage);
}

void UHealthComponent::SetCurrentHP(float Value)
{
	CurrentHealth = Value;
	
	if(CurrentHealth > MaxHealth)
	{
		CurrentHealth = MaxHealth;
	}

	if(CurrentHealth <= 0)
	{
		CurrentHealth = 0;

		if(OnEmptyHealth.IsBound())
		{
			OnEmptyHealth.Broadcast();
		}
	}
}

