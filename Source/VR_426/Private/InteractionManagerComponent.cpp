// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionManagerComponent.h"
#include "ContainerObject.h"
#include "WearponComponent.h"
#include "WearponObject.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UInteractionManagerComponent::UInteractionManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

UContainerObject* UInteractionManagerComponent::CreateContainerObject(AActor* Item)
{
	UContainerObject* obj = nullptr;

	UItemComponent* itemComp = Item->FindComponentByClass<UItemComponent>();

	if(!itemComp)
	{
		return nullptr;
	}

	if(!itemComp->ContainerObjectClass)
	{
		return nullptr;
	}

	obj = Cast<UContainerObject>(UGameplayStatics::SpawnObject(itemComp->ContainerObjectClass, this));
	
	if(obj)
	{
		obj->ItemClass = Item->GetClass();
		obj->Owner = GetOwner();
	}

	return obj;
}

void UInteractionManagerComponent::ManageItem_Implementation(AActor* Item)
{
}



// Called when the game starts
void UInteractionManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UInteractionManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

