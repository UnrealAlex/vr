// Fill out your copyright notice in the Description page of Project Settings.


#include "StackableItemComponent.h"

#include "StackableObject.h"

int UStackableItemComponent::GetItems(int GetCount)
{
    int getCount = GetCount;
    int itemsCount = ItemsCount;

    ItemsCount -= GetCount;

    if(ItemsCount <= 0)
    {
        DestroyItem();
        
        return itemsCount;
    }

    return getCount;
}

int UStackableItemComponent::AddItems(int AddCount)
{
    return 0;
}

UContainerObject* UStackableItemComponent::CreateContainerObj()
{
    UContainerObject* obj = Super::CreateContainerObj();

    if(UStackableObject* stackObj = Cast<UStackableObject>(obj))
    {
        stackObj->AddItems(GetItems(stackObj->StackSize - stackObj->ItemsCount));
        return stackObj;
    }

    return nullptr;
}
