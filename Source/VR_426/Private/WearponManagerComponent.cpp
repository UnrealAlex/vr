// Fill out your copyright notice in the Description page of Project Settings.


#include "WearponManagerComponent.h"
#include "WearponComponent.h"
#include "ContainerObject.h"
#include "WearponObject.h"
#include "InventoryComponent.h"
#include "AmmoObject.h"
#include "InventoryManagerComponent.h"

// Sets default values for this component's properties
UWearponManagerComponent::UWearponManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	CurrentWearponId = -1;
	//WearponSlotsCount = 2;
	// ...
}

bool UWearponManagerComponent::AttachItemToSocket(AActor* Item, UMeshComponent* ParentComponent, FName SocketName)
{
	UMeshComponent* itemMesh = Item->FindComponentByClass<UMeshComponent>();

	if(!itemMesh || !ParentComponent)
	{
		return false;
	}

	UWearponComponent* wearponComp = Item->FindComponentByClass<UWearponComponent>();

	if(itemMesh)
	{
		itemMesh->SetSimulatePhysics(false);
		itemMesh->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel18);
		wearponComp->CharacterMesh = CharacterMesh;
	}

	itemMesh->AttachToComponent(ParentComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);

	return true;
};

bool UWearponManagerComponent::DetachItem(AActor* Item)
{
	UMeshComponent* itemMesh = Item->FindComponentByClass<UMeshComponent>();

	if(!itemMesh)
	{
		return false;
	}

	itemMesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);

	itemMesh->SetSimulatePhysics(true);
	itemMesh->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);

	itemMesh->AddImpulse(itemMesh->GetForwardVector() * 200.f, NAME_None, true);

	bWearponEquipped = false;
	
	return true;
}

void UWearponManagerComponent::EquipWearpon_Implementation(UContainerObject* Item)
{
	if(!Item || !WearponContainer)
	{
		return;
	}

	CurrentWearponId = WearponContainer->Items.Find(Item); //WearponSlots.Find(Item);
	
	FVector Location = GetOwner()->GetActorLocation();
	
	TArray<USkeletalMeshComponent*> meshes;
	GetOwner()->GetComponents<USkeletalMeshComponent>(meshes);

	USkeletalMeshComponent** characterMeshPtr = nullptr;
	
	if(meshes.Num() > 0)
	{
		characterMeshPtr = meshes.FindByPredicate([&] (USkeletalMeshComponent* sm)
        {
            return sm->ComponentTags.Contains(CharacterMeshTag);
        });
	}

	if(!characterMeshPtr)
	{
		return;
	}

	CharacterMesh = *characterMeshPtr;

	FActorSpawnParameters spawnParams;
	spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	
	CurrentWearpon = GetWorld()->SpawnActor<AActor>(Item->ItemClass, Location, FRotator(0.f, 0.f, 0.f), spawnParams);

	CurrentWearpon->SetOwner(GetOwner());
	CurrentWearpon->SetActorHiddenInGame(true);
	
	UWearponComponent* wearponComp = CurrentWearpon->FindComponentByClass<UWearponComponent>();

	if(wearponComp)
	{
		Cast<UWearponObject>(Item)->GetClipParams(wearponComp->ClipSize, wearponComp->BulletCount);
	}
		
	AttachItemToSocket(CurrentWearpon, CharacterMesh, WearponSocketName);
	CurrentWearpon->SetActorHiddenInGame(false);

	float equipTimerDelay = wearponComp->EquipMontage->CalculateSequenceLength();
	
	GetWorld()->GetTimerManager().SetTimer(AttachTimer, [&]()
	{
		OnEquipWearpon.Broadcast(CurrentWearpon);
		bWearponEquipped = true;
	}, equipTimerDelay, false, equipTimerDelay);
}

void UWearponManagerComponent::UnequipWearpon_Implementation()
{

	if(!CurrentWearpon || !WearponContainer)
	{
		return;
	}
	
	UContainerObject** hideObjPtr = WearponContainer->Items.FindByPredicate([&](UContainerObject* obj) //WearponSlots.FindByPredicate([&](UContainerObject* obj)
	{
		return CurrentWearpon->GetClass() == obj->ItemClass;
	});

	UContainerObject* hideObj = nullptr;

	if(hideObjPtr)
	{
		hideObj = *hideObjPtr;
	}

	if(!hideObj)
	{
		return;
	}

	if(OnUnequipWearpon.IsBound())
	{
		OnUnequipWearpon.Broadcast(CurrentWearpon);
	}

	UWearponComponent* wearponComp = CurrentWearpon->FindComponentByClass<UWearponComponent>();

	if(wearponComp)
	{
		Cast<UWearponObject>(hideObj)->AddClip(wearponComp->ClipSize, wearponComp->BulletCount);
		wearponComp->CharacterMesh = nullptr;
	}

	bWearponEquipped = false;

	float unequipTimerDelay = wearponComp->UnequipMontage->CalculateSequenceLength();

	GetWorld()->GetTimerManager().SetTimer(DetachTimer, [&]()
    {
		if(CurrentWearpon)
		{
            CurrentWearpon->Destroy();
        }
	
        CurrentWearpon = nullptr;
		
    }, unequipTimerDelay - 0.1f, false, unequipTimerDelay - 0.1f);
	
}

bool UWearponManagerComponent::CanAdd()
{
	if(!WearponContainer)
	{
		return false;
	}
	
	return  WearponContainer->Items.Num() <  WearponContainer->MaxItemsCount; //WearponSlots.Num() < WearponSlotsCount;
}

bool UWearponManagerComponent::Add_Implementation(AActor* Item)
{
	if(CanAdd())
	{
		UItemComponent* itemComp = Item->FindComponentByClass<UItemComponent>();

		if(!itemComp)
		{
			return false;
		}

		UContainerObject* obj = itemComp->CreateContainerObject();

		if(!obj)
		{
			return false;
		}
		
		WearponContainer->AddItem(obj); //WearponSlots.Add(obj);

		if(OnAddWearpon.IsBound())
		{
			OnAddWearpon.Broadcast(obj);
		}

		itemComp->DestroyItem();
		
		return true;
	}

	return false;
}

bool UWearponManagerComponent::Delete_Implementation(UContainerObject* Item)
{
	// int removedCount = WearponSlots.Remove(Item);
	//
	// if(removedCount > 0)
	// {
	// 	return true;
	// }
	//
	// return false;

	return WearponContainer->RemoveItem(Item); 
}

void UWearponManagerComponent::SetCurrentPrew()
{
	if(!CanChangeWearpon())
	{
		return;
	}

	if(CurrentWearponId == 0)
	{
		CurrentWearponId = WearponContainer->Items.Num() - 1; //WearponSlots.Num() - 1;
	}
	else
	{
		CurrentWearponId = CurrentWearponId - 1;
	}

	ChangeWearpon(CurrentWearponId);	
	
	if(OnChangeCurrentWearpon.IsBound())
	{
		OnChangeCurrentWearpon.Broadcast(CurrentWearponId);
	}
}

void UWearponManagerComponent::ChangeWearpon(int WearponId)
{
	if(CurrentWearpon)
	{
		UWearponComponent* wearponComp = CurrentWearpon->FindComponentByClass<UWearponComponent>();
		float unequipTimerDelay = wearponComp->UnequipMontage->CalculateSequenceLength();
		
		UnequipWearpon();

		GetWorld()->GetTimerManager().SetTimer(AttachTimer, [&, WearponId]()
        {
            EquipWearpon(WearponContainer->Items[WearponId]); //WearponSlots[WearponId]);
        }, unequipTimerDelay, false, unequipTimerDelay);

		return;
	}

	EquipWearpon(WearponContainer->Items[WearponId]); //WearponSlots[WearponId]);
}

void UWearponManagerComponent::DropCurrentWearpon()
{
	
	Delete(WearponContainer->Items[CurrentWearponId]); //WearponSlots[CurrentWearponId]);

	FTimerHandle DropTimer;
	GetWorld()->GetTimerManager().SetTimer(DropTimer, [&]()
	{
		DetachItem(CurrentWearpon);
		CurrentWearpon = nullptr;
	}, 0.2f, false, 0.2f);
	
}

float UWearponManagerComponent::GetMontageTime(UAnimMontage* Montage)
{
	if(!Montage)
	{
		return  0.f;
	}
	
	return  Montage->CalculateSequenceLength();
}

bool UWearponManagerComponent::CanChangeWearpon()
{
	if(!WearponContainer)
	{
		return false;
	}
		
	if(WearponContainer->Items.Num() > 0) //WearponSlots.Num() > 0)
	{
		return true;
	}

	return false;
}

void UWearponManagerComponent::SetCurrentNext()
{
	if(!CanChangeWearpon())
	{
		return;
	}

	if(CurrentWearponId == WearponContainer->Items.Num() - 1) //WearponSlots.Num() - 1)
	{
		CurrentWearponId = 0;
	}
	else
	{
		CurrentWearponId = CurrentWearponId + 1;
	}

	ChangeWearpon(CurrentWearponId);
	
	if(OnChangeCurrentWearpon.IsBound())
	{
		OnChangeCurrentWearpon.Broadcast(CurrentWearponId);
	}
}

void UWearponManagerComponent::Reload()
{
	if(!CurrentWearpon)
	{
		return;
	}

	UWearponComponent* wearponComp = CurrentWearpon->FindComponentByClass<UWearponComponent>();

	if(!wearponComp)
	{
		return;
	}

	if(wearponComp->BulletTypes.Num() == 0 || wearponComp->BulletCount == wearponComp->ClipSize)
	{
		return;
	}

	UInventoryManagerComponent* inventoryManager = GetOwner()->FindComponentByClass<UInventoryManagerComponent>();

	if(!inventoryManager)
	{
		return;
	}

	UAmmoObject* ammo = nullptr;
	//UAmmoObject* a = nullptr;

	TArray<UContainerObject*> ammoObjs = inventoryManager->GetObjectsByClass(UAmmoObject::StaticClass());

	UContainerObject** ammoObjPtr = ammoObjs.FindByPredicate([&](UContainerObject* obj)
	{
		UAmmoObject* a = Cast<UAmmoObject>(obj);

                if(a)
                {
                    if(a->AmmoName == wearponComp->BulletTypes[wearponComp->CurrentBulletTypeId])
                    {
                         return true;
                    }
                }
				
                return false;
	});

	if(!ammoObjPtr)
	{
		ammoObjPtr = ammoObjs.FindByPredicate([&](UContainerObject* obj)
        {
            UAmmoObject* a = Cast<UAmmoObject>(obj);

            if(a)
            {
                int idx = wearponComp->BulletTypes.Find(a->AmmoName);
					
                if(idx != INDEX_NONE)
                {
                    if(idx != wearponComp->CurrentBulletTypeId)
                    {
                        if(wearponComp->CurrentBulletObj)
                        {
                            OnUnloadWearpon.Broadcast(wearponComp->Unload());
                        }
                        wearponComp->SetCurrentBulletTypeID(idx);
                    }
                    return true;
                }
            }
            return false;
        });
	}
	
	if(ammoObjPtr)
	{
		ammo = Cast<UAmmoObject>(*ammoObjPtr);
	}

	if(!ammo)
	{
		return;
	}

	wearponComp->Reload(ammo);

	if(OnReloadWearpon.IsBound())
	{
		OnReloadWearpon.Broadcast();
	}
}

// Called when the game starts
void UWearponManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	TArray<UInventoryComponent*> comps;

	GetOwner()->GetComponents<UInventoryComponent>(comps);

	UInventoryComponent** compPtr = comps.FindByPredicate([&](UInventoryComponent* c)
	{
		return c->ComponentTags.Contains(WearponComtainerTag);
	});

	if(compPtr)
	{
		WearponContainer = *compPtr;
	}
	
}


// Called every frame
void UWearponManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

