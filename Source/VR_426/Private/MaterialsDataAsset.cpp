// Fill out your copyright notice in the Description page of Project Settings.


#include "MaterialsDataAsset.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

FMaterialData UMaterialsDataAsset::GetDataByMaterial(UPhysicalMaterial* material)
{
    UPhysicalMaterial* m = material;

    FMaterialData* data = MaterialTypes.FindByPredicate([m](FMaterialData d)
        {
            return d.PhysicalMaterial == m;
        });

    if (data)
    {
        return *data;
    }

    return FMaterialData();
}
