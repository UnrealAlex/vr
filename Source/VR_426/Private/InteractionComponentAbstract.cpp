// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionComponentAbstract.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values for this component's properties
UInteractionComponentAbstract::UInteractionComponentAbstract()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;

	// ...
}


// Called when the game starts
void UInteractionComponentAbstract::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UInteractionComponentAbstract::Focused()
{
}

void UInteractionComponentAbstract::FocusLost()
{
}


// Called every frame
void UInteractionComponentAbstract::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	Focused();

	FocusLost();
}

void UInteractionComponentAbstract::InitializeComponent()
{
}