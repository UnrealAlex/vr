// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthManagerComponent.h"

#include "CharacterHealthComponent.h"
#include "CharacterPartHealthComponent.h"

// Sets default values for this component's properties
UHealthManagerComponent::UHealthManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHealthManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle tmpTimerHandle;

	GetWorld()->GetTimerManager().SetTimer(tmpTimerHandle, [&]()
	{
		
		GetOwner()->GetComponents<UCharacterHealthComponent>(healthComponents);

		for(auto c : healthComponents)
		{
			c->OnBleeding.AddUniqueDynamic(this, &UHealthManagerComponent::OnBleeding);
		}

		MainHealthComponent = *(healthComponents.FindByPredicate([&](UCharacterHealthComponent* c)
		{
			return c->ComponentHasTag(MainHealthComponentTag);
		}));

		if(MainHealthComponent)
		{
			MainHealthComponent->OnEmptyHealth.AddUniqueDynamic(this, &UHealthManagerComponent::OnEmptyHealth);
		}

        GetOwner()->GetComponents<UCharacterPartHealthComponent>(partHealthComponents);

        for(auto p : partHealthComponents)
        {
            p->OnDetachPart.AddUniqueDynamic(this, &UHealthManagerComponent::OnPartDetached);
        	p->OnEmptyPartHealth.AddUniqueDynamic(this, &UHealthManagerComponent::OnEmptyPartHealth);
        }
		
	}, 0.1f, false, 0.1f);
	
}


// Called every frame
void UHealthManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}


UCharacterPartHealthComponent* UHealthManagerComponent::GetPartHealthByBoneName(FName BoneName)
{
	return nullptr;
}

void UHealthManagerComponent::ApplyDamage(float Damage, FName BoneName,
	UPrimitiveComponent* HitComponent, FVector HitLocation)
{
	if(!MainHealthComponent)
	{
		return;
	}

	float damageMod = 1.f;

	UCharacterPartHealthComponent** PartCompPtr = partHealthComponents.FindByPredicate([BoneName](UCharacterPartHealthComponent* p)
	{
		return p->BoneName == BoneName;
	});

	UCharacterPartHealthComponent* PartComp;
	
	if(PartCompPtr)
	{
		PartComp = *PartCompPtr;
		PartComp->ApplyDamage(Damage, HitComponent, HitLocation);

		damageMod = PartComp->DamageModifier;
	}
	
	MainHealthComponent->ApplyDamage(Damage * damageMod, HitComponent, HitLocation);
}

void UHealthManagerComponent::OnEmptyHealth_Implementation()
{
	for(auto c : partHealthComponents)
	{
		c->StopBleeding();
	}
}

void UHealthManagerComponent::OnEmptyPartHealth_Implementation(FName BoneName)
{
	
}

void UHealthManagerComponent::OnBleeding_Implementation(float BleedingIntencity)
{
	
}

void UHealthManagerComponent::OnPartDetached_Implementation(FName BoneName)
{
	USkeletalMeshComponent* mesh = GetOwner()->FindComponentByClass<USkeletalMeshComponent>();

	if(!mesh)
	{
		return;
	}

	FName nextBoneName = mesh->GetBoneName(mesh->GetBoneIndex(BoneName) + 1);

	UCharacterPartHealthComponent** partHealthPtr = partHealthComponents.FindByPredicate([nextBoneName](UCharacterPartHealthComponent* p)
    {
        return p->BoneName == nextBoneName;
    });

	UCharacterPartHealthComponent* PartComp;
	
	if(partHealthPtr)
	{
		PartComp = *partHealthPtr;

		PartComp->StopBleeding();
	}
}

