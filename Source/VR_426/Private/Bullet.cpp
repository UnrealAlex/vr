// Fill out your copyright notice in the Description page of Project Settings.


#include "Bullet.h"
#include "BallisticFunctionLibrary.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "BallisticComponent.h"



UBullet::UBullet()
{

}

void UBullet::Destroy()
{
	ConditionalBeginDestroy();
}

void UBullet::Fire()
{
	UObject* worldContext = GetOuter();

	if (!worldContext)
	{
		return;
	}

	Points.Empty();
	TraceCounter = 0;
	deltaTime = 0;

	Direction = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(Direction, Dispersion);

	FPredictProjectilePathParams params;
	params.StartLocation = StartLocation;
	params.LaunchVelocity = Direction * LaunchVelocity * 100;
	params.bTraceWithCollision = false;
	params.bTraceWithChannel = false;
	params.ProjectileRadius = 1.f;
	params.MaxSimTime = MaxSimTime;
	params.SimFrequency = 15.f;
	params.DrawDebugTime = 0.f;
	params.DrawDebugTime = EDrawDebugTrace::None;

	if (bMultyProjectile)
	{
		for (int i = 0; i < ProjectileData.ProjectileCount; i++)
		{
			UBullet* proj = NewObject<UBullet>(worldContext);
			proj->ProjectileData = ProjectileData;
			proj->Direction = Direction;
			proj->Dispersion = Dispersion;
			proj->StartLocation = StartLocation;
			proj->MaterialsData = MaterialsData;
			proj->LaunchVelocity = LaunchVelocity;
			proj->bMultyProjectile = false;
			proj->MaxSimTime = 1.f;
			proj->SetBallisticComponent(BallisticComponent);
			proj->DestroyTimer = 1.f;
			proj->IgnoredActors = IgnoredActors;

			proj->OnProjectileHit.AddUniqueDynamic(this, &UBullet::_ProjectileHit);

			proj->Fire();

			projectiles.Add(proj);
		}
		
	}
	else
	{
		UBallisticFunctionLibrary::SimplePredictProjectilePath(worldContext, ProjectileData, WindComponent, params, result);
		LineTrace();
	}
	
	FTimerHandle destroytimer;

	GetWorld()->GetTimerManager().SetTimer(destroytimer, this, &UBullet::Destroy, DestroyTimer, false);

}

void UBullet::LineTrace()
{
	UWorld* world = GetWorld();

	if (!world)
	{
		return;
	}

	FPredictProjectilePathPointData currentPoint;
	FPredictProjectilePathPointData nextPoint;
	
	float traceLength;

	if (TraceCounter + 1 < result.PathData.Num())
	{
		currentPoint = result.PathData[TraceCounter];
		nextPoint = result.PathData[TraceCounter + 1];
		traceLength = FVector::Dist(currentPoint.Location, nextPoint.Location);

		

		FCollisionQueryParams queryParams;
		queryParams.bReturnPhysicalMaterial = true;
		queryParams.bTraceComplex = true;
		queryParams.AddIgnoredActors(IgnoredActors);

		FCollisionObjectQueryParams objectQueryParams;
		for(TEnumAsByte<ECollisionChannel> t : ProjectileData.ObjectTypes)
		{
			objectQueryParams.AddObjectTypesToQuery(t);
		}
		


		///todo change lineTraceByChannel to lineTraceForObject

		world->LineTraceSingleByObjectType(Hit, currentPoint.Location, nextPoint.Location, objectQueryParams, queryParams);
		//world->LineTraceSingleByChannel(Hit, currentPoint.Location, nextPoint.Location, ProjectileData.CollisionChannel, queryParams);

		deltaTime = nextPoint.Time - currentPoint.Time;

		Speed = (traceLength / deltaTime);

		PathTrailComp = UGameplayStatics::SpawnEmitterAtLocation(world, BallisticComponent->PathTrail, currentPoint.Location);

		if (PathTrailComp)
		{
			PathTrailComp->SetVectorParameter(BallisticComponent->PathTrailTargetName, nextPoint.Location);
		}

		// Debug /////////////////////////////////////////////////////////////////////////////
		if (BallisticComponent->bDebug)
		{
			//color
			int ri = 254 * (Speed / (ProjectileData.StartSpeed * 100));
			int gi = 254 * (1 - (Speed / (ProjectileData.StartSpeed * 100)));

			if (Hit.bBlockingHit)
			{
				DrawDebugLine(world, currentPoint.Location, Hit.Location, FColor::FColor(ri, gi, 0), false, 10.f, 0, 3.f);
			}
			else
			{
				DrawDebugLine(world, currentPoint.Location, nextPoint.Location, FColor::FColor(ri, gi, 0), false, 10.f, 0, 3.f);
			}
		}
		// End debug ///////////////////////////////////////////////////////////////////////

		// hit
		if (Hit.bBlockingHit)
		{
			if (PathTrailComp)
			{
				PathTrailComp->DeactivateSystem();
			}

			FVector direction = UKismetMathLibrary::GetDirectionUnitVector(Hit.TraceStart, Hit.TraceEnd);
			float angle = FMath::RadiansToDegrees(FMath::Acos(FVector::DotProduct(direction.GetSafeNormal(), Hit.Normal)));

			FMaterialData materialData;

			UPhysicalMaterial* pMat = Hit.PhysMaterial.Get();

			if (MaterialsData)
			{
				materialData = MaterialsData->GetDataByMaterial(pMat);
			}

			float barrierThickness;
			FVector breackLocation;

			if (CheckBreaking(Hit, direction, angle, materialData, barrierThickness, breackLocation))
			{
				CurrentDistance += (Hit.Location - Hit.TraceStart).Size();

				Break(materialData, barrierThickness, breackLocation);
				
				return;
			}
			else
			{
				if (CheckRicochet(materialData, angle))
				{
					CurrentDistance += (Hit.Location - Hit.TraceStart).Size();
					Ricochet(Hit, direction, angle, materialData);
					return;
				}
				else
				{
					// event
					CurrentDistance += (Hit.Location - Hit.TraceStart).Size();

					FProjectileHitResult projHit;
					projHit.Distance = CurrentDistance / 100.f;
					projHit.Force = (((FMath::Square((Speed / 100.f)) * (ProjectileData.Weight / 1000))) / 2);
					projHit.BarrierBreakingForce = BarrierBreakingForce;
					projHit.Speed = Speed / 100.f;
					projHit.IsBreaking = false;
					projHit.IsRicochet = false;
					projHit.MaterialData = materialData;

					_ProjectileHit(direction * -1, ProjectileData, projHit, Hit);
					return;
				}
			}
			return;
		}

		CurrentTime += deltaTime;
		CurrentDistance += traceLength;

		world->GetTimerManager().SetTimer(Timer, this, &UBullet::LineTrace, deltaTime, false);
		TraceCounter++;
	}
}

bool UBullet::CheckBreaking(FHitResult HitResult, FVector direction, float angle, FMaterialData materialData, float& barrierThickness, FVector& breakLocation)
{
	UWorld* world = GetWorld();

	if (!world)
	{
		return false;
	}

	FVector startPoint = HitResult.Location;
	
	float distance = 0;

	TArray<float> thickness;
	thickness.Add(1.f);
	thickness.Add(5.f);
	thickness.Add(10.f);
	thickness.Add(15.f);
	thickness.Add(20.f);
	thickness.Add(30.f);
	thickness.Add(40.f);
	thickness.Add(50.f);
	thickness.Add(100.f);

	FHitResult hit;

	//Check breacking

	for (float f : thickness)
	{
		FVector CheckTraceStart = startPoint + direction * f;
		FVector CheckTraceEnd = CheckTraceStart + direction * -1 * f;


		FCollisionQueryParams queryParams;
		queryParams.bReturnPhysicalMaterial = true;
		queryParams.bTraceComplex = true;
		queryParams.AddIgnoredActors(IgnoredActors);

		FCollisionObjectQueryParams objectQueryParams;
		for(TEnumAsByte<ECollisionChannel> t : ProjectileData.ObjectTypes)
		{
			objectQueryParams.AddObjectTypesToQuery(t);
		}
		


		///todo change lineTraceByChannel to lineTraceForObject

		world->LineTraceSingleByObjectType(hit, CheckTraceStart, CheckTraceStart, objectQueryParams, queryParams);

		

		//world->LineTraceSingleByChannel(hit, CheckTraceStart, CheckTraceStart, ECollisionChannel::ECC_Visibility);
		if (hit.bBlockingHit)
		{
			distance = f;
			break;
		}

		// Debug /////////////////////////////////////////////////////////////////////////////
		if (BallisticComponent->bDebug)
		{
			DrawDebugLine(world, CheckTraceStart, CheckTraceEnd, FColor::Blue, false, 10.f, 0, 3.f);
		}
		// End debug ///////////////////////////////////////////////////////////////////////
	}

	// if can break
	if (hit.bBlockingHit)
	{
		// set barrier thickness
		barrierThickness = distance - hit.Distance;

		if (!materialData.IsValid())
		{
			return false;
		}

		// calculate the velocity to break through the barrier
		float brackingSpeed = (materialData.StrengthCoefficient * ((barrierThickness * 0.7f * (1 / ProjectileData.Caliber * 0.75f) / (ProjectileData.Weight * 0.5 * FMath::Cos(FMath::DegreesToRadians(180 - angle)))))) * 100;

		// calculate the force to break through the barrier
		BarrierBreakingForce = (FMath::Square((brackingSpeed / 100.f)) * 3) / 100.f;
		float projectileForce = (((FMath::Square((Speed / 100.f)) * (ProjectileData.Weight / 1000))) / 2) * ProjectileData.ArmorPiercingCoefficient;

		if (BarrierBreakingForce > projectileForce)
		{
			return false;
		}
		else
		{
			breakLocation = hit.Location;
			return true;
		}
	}
	return false;
}

bool UBullet::CheckRicochet(FMaterialData materialData, float angle)
{
	if (materialData.bCanRicochet && (180 - angle) > (90.f - materialData.RicochetMaxAngle))
	{
		return true;
	}
	return false;
}

void UBullet::Break(FMaterialData materialData, float barrierThickness, FVector location)
{
	float decelerationCoefficient = (materialData.DecelerationCoefficient * 1000.f) * barrierThickness;

	Dispersion = barrierThickness * materialData.DispersionCoefficient;
	if (barrierThickness < 0.1)
	{
		StartLocation = location + Direction * 2;
	}
	else
	{
		StartLocation = location;
	}
	
	LaunchVelocity = (Speed - (decelerationCoefficient / ProjectileData.ArmorPiercingCoefficient)) / 100.f;

	FVector direction = UKismetMathLibrary::GetDirectionUnitVector(Hit.TraceStart, Hit.TraceEnd);

	FProjectileHitResult projHit;
	projHit.Distance = CurrentDistance / 100.f;
	projHit.Force = (((FMath::Square((Speed / 100.f)) * (ProjectileData.Weight / 1000))) / 2);
	projHit.BarrierBreakingForce = BarrierBreakingForce;
	projHit.Speed = Speed / 100.f;
	projHit.IsBreaking = true;
	projHit.IsRicochet = false;
	projHit.MaterialData = materialData;

	_ProjectileHit(direction * -1, ProjectileData, projHit, Hit);

	//_ProjectileHit(direction, ProjectileData, projHit, Hit);

	Fire();

}                                                     

void UBullet::Ricochet(FHitResult HitResult, FVector direction, float angle, FMaterialData materialData)
{
	FVector ricochetDirection;

	ricochetDirection = UKismetMathLibrary::MirrorVectorByNormal(direction, HitResult.Normal);

	float angleCoefficient = FMath::Sin(FMath::DegreesToRadians(180 - angle));

	Direction = ricochetDirection;
	Dispersion = materialData.RicochetDispersionCoefficient;
	StartLocation = HitResult.Location;
	LaunchVelocity = Speed * angleCoefficient * materialData.RicochetDecelerationCoefficient / 100.f;

	FProjectileHitResult projHit;
	projHit.Distance = CurrentDistance / 100.f;
	projHit.Force = (((FMath::Square((Speed / 100.f)) * (ProjectileData.Weight / 1000))) / 2) * angleCoefficient;
	projHit.BarrierBreakingForce = BarrierBreakingForce;
	projHit.Speed = Speed / 100.f;
	projHit.IsBreaking = false;
	projHit.IsRicochet = true;
	projHit.MaterialData = materialData;

	_ProjectileHit(direction * -1, ProjectileData, projHit, Hit);

	Fire();
}

void UBullet::_ProjectileHit(FVector HitFromDirection, FProjectileData projectileData, FProjectileHitResult ProjectileHitResult, FHitResult HitResult)
{
	OnProjectileHit.Broadcast(HitFromDirection, projectileData, ProjectileHitResult, HitResult);
}

void UBullet::SetBallisticComponent(UBallisticComponent* component)
{
	BallisticComponent = component;
}

UBallisticComponent* UBullet::GetBallisticComponent()
{
	return BallisticComponent;
}

UWorld* UBullet::GetWorld() const
{
	if (GEngine)
	{
		return GEngine->GetWorldFromContextObjectChecked(GetOuter());
	}

	return nullptr;
}
