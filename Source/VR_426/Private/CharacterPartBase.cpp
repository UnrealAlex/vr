// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterPartBase.h"

// Sets default values
ACharacterPartBase::ACharacterPartBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PartMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("PartMesh"));
	PartMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ACharacterPartBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACharacterPartBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

