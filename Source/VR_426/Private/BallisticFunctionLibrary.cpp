// Fill out your copyright notice in the Description page of Project Settings.


#include "BallisticFunctionLibrary.h"
#include "Components/WindDirectionalSourceComponent.h"
#include "Bullet.h"

void UBallisticFunctionLibrary::SimplePredictProjectilePath(const UObject* WorldContextObject, FProjectileData ProjectileData, UWindDirectionalSourceComponent* WindComponent, const FPredictProjectilePathParams& PredictParams, FPredictProjectilePathResult& PredictResult)
{
	PredictResult.Reset();

	UWorld const* const World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (World && PredictParams.SimFrequency > KINDA_SMALL_NUMBER)
	{
		const float SubstepDeltaTime = 1.f / PredictParams.SimFrequency;
		const float GravityZ = FMath::IsNearlyEqual(PredictParams.OverrideGravityZ, 0.0f) ? World->GetGravityZ() : PredictParams.OverrideGravityZ;
		const float ProjectileRadius = PredictParams.ProjectileRadius;

		FVector CurrentVel = PredictParams.LaunchVelocity;
		FVector TraceStart = PredictParams.StartLocation;
		FVector TraceEnd = TraceStart;
		float CurrentTime = 0.f;
		PredictResult.PathData.Reserve(FMath::Min(128, FMath::CeilToInt(PredictParams.MaxSimTime * PredictParams.SimFrequency)));
		PredictResult.AddPoint(TraceStart, CurrentVel, CurrentTime);

		const float MaxSimTime = PredictParams.MaxSimTime;
		while (CurrentTime < MaxSimTime)
		{
			// Limit step to not go further than total time.
			const float PreviousTime = CurrentTime;
			const float ActualStepDeltaTime = FMath::Min(MaxSimTime - CurrentTime, SubstepDeltaTime);
			CurrentTime += ActualStepDeltaTime;

			float Speed = CurrentVel.Size();

			float DecelerationF;

			if (Speed > 34300)
			{
				DecelerationF = ProjectileData.BallisticCoefficient * 4.f * (Speed - (Speed / 2.f * (1.f - (Speed / (ProjectileData.StartSpeed * 100.f))))) * ActualStepDeltaTime;
			}
			else
			{
				DecelerationF = (FMath::Square(Speed) * 0.0000121f) * ActualStepDeltaTime; 
			}

			FVector Deceleration = (CurrentVel.GetSafeNormal() * DecelerationF); 

			// Integrate (Velocity Verlet method)
			TraceStart = TraceEnd;
			FVector OldVelocity = CurrentVel;

			CurrentVel = OldVelocity + FVector(0.f, 0.f, GravityZ * ActualStepDeltaTime);

			if (WindComponent)
			{
				FVector WindEffect = WindComponent->GetForwardVector() * (WindComponent->Speed * ActualStepDeltaTime * (1.f - (Speed / (ProjectileData.StartSpeed * 100.f))));
				CurrentVel += WindEffect;
			}


			CurrentVel -= (Deceleration);

			TraceEnd = TraceStart + (OldVelocity + CurrentVel) * (0.5f * ActualStepDeltaTime);

			PredictResult.LastTraceDestination.Set(TraceEnd, CurrentVel, CurrentTime);

			PredictResult.AddPoint(TraceEnd, CurrentVel, CurrentTime);
		}
	}

}