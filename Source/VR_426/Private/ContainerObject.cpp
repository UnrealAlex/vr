// Fill out your copyright notice in the Description page of Project Settings.


#include "ContainerObject.h"


AActor* UContainerObject::DropItem_Implementation(FVector Location, FRotator Rotation)
{
    return Drop(Location, Rotation);
}

AActor* UContainerObject::Drop(FVector Location, FRotator Rotation)
{
    if(!Owner)
    {
        return nullptr;
    }
    
    FActorSpawnParameters spawnParams;
    spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    ///todo add Destroy() in timer?

    return Owner->GetWorld()->SpawnActor<AActor>(ItemClass, Location, Rotation, spawnParams);
}

void UContainerObject::Destroy()
{
    if(OnDestroyObject.IsBound())
    {
        OnDestroyObject.Broadcast(this);
    }
    ConditionalBeginDestroy();
}
