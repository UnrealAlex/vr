// Fill out your copyright notice in the Description page of Project Settings.


#include "BulletsDataAsset.h"

FProjectileData UBulletsDataAsset::GetDataByName(FString name)
{
    FString n = name;

    FProjectileData* data = ProjectileTypes.FindByPredicate([n](FProjectileData d)
        {
            return d.Name == n;
        });

    if (data)
    {
        return *data;
    }

    return FProjectileData();
}