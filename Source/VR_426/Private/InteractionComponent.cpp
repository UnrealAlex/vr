// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "ItemInteractionComponent.h"

void UInteractionComponent::Focused()
{
	FVector start = Camera->GetComponentLocation();
	FVector end = start + Camera->GetForwardVector() * TraceLenght;
	
	UKismetSystemLibrary::SphereTraceSingleForObjects(GetWorld(), start, end, TraceRadius, ObjectTypes,
        false, IgnoredActors, EDrawDebugTrace::None, Hit, true);
	
	if(!FocusedItem || Hit.GetActor() != FocusedItem)
	{
		if(Hit.GetActor())
		{
			FocusedItem = Hit.GetActor();

			if(OnFocused.IsBound())
			{
				OnFocused.Broadcast(FocusedItem);
			}
		}
	}
}

void UInteractionComponent::FocusLost()
{
	if(FocusedItem && !Hit.GetActor())
	{
		FocusedItem = nullptr;

		if(OnFocusLost.IsBound())
		{
			OnFocusLost.Broadcast();
		}
	}
}

void UInteractionComponent::InteractEvent_Implementation()
{
	if(!FocusedItem)
	{
		return;
	}

	UItemInteractionComponent* itemComp = FocusedItem->FindComponentByClass<UItemInteractionComponent>();

	if(itemComp)
	{
		itemComp->Interact(GetOwner());
	}
}

void UInteractionComponent::InitializeComponent()
{
	Super::InitializeComponent();
	
	Camera = GetOwner()->FindComponentByClass<UCameraComponent>();

	IgnoredActors.Add(GetOwner());
}
