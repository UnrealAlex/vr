// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryManagerComponent.h"
#include "InventoryComponent.h"
#include "StackableItemComponent.h"
#include "StackableObject.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UInventoryManagerComponent::UInventoryManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UInventoryManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

TArray<UInventoryComponent*> UInventoryManagerComponent::GetInventorys()
{
	TArray<UInventoryComponent*> components;
	GetOwner()->GetComponents<UInventoryComponent>(components);

	return components;
}

UInventoryComponent* UInventoryManagerComponent::GetInventoryByAllowObjectClasses(
	TSubclassOf<UContainerObject> ObjectClass)
{
	TArray<UInventoryComponent*> components = GetInventorys();

	UInventoryComponent** compPtr = components.FindByPredicate([&](UInventoryComponent* c)
	{
		return  c->CanAddItemByClass(ObjectClass);
	});

	if(!compPtr)
	{
		return nullptr;
	}

	return *compPtr;
}

TArray<UContainerObject*> UInventoryManagerComponent::GetObjectsByClass(TSubclassOf<UContainerObject> ObjectClass)
{
	TArray<UContainerObject*> objs;

	for(auto inventory : GetInventorys())
	{
		for(auto obj : inventory->Items)
		{
			if( obj->GetClass() == ObjectClass || obj->GetClass()->IsChildOf(ObjectClass))
			{
				objs.Add(obj);
			}
		}
	}

	return objs;
}


// Called every frame
void UInventoryManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UContainerObject* UInventoryManagerComponent::AddItem(AActor* Item)
{
	UItemComponent* itemComponent = Item->FindComponentByClass<UItemComponent>();

	UContainerObject* obj = itemComponent->CreateContainerObject();

	UInventoryComponent* inventory = GetInventoryByAllowObjectClasses(obj->GetClass());

	if(inventory->AddItem(obj))
	{
		itemComponent->DestroyItem();
		return obj;
	}

	return nullptr;
}

UContainerObject* UInventoryManagerComponent::AddObject(UContainerObject* Obj)
{
	if(!Obj)
	{
		return nullptr;
	}
	
	UInventoryComponent* inventory = GetInventoryByAllowObjectClasses(Obj->GetClass());

	if(!inventory)
	{
		return nullptr;
	}
	
	inventory->AddItem(Obj);

	return Obj;
}

bool UInventoryManagerComponent::RemoveObject(UContainerObject* Obj)
{
	TArray<UInventoryComponent*> Inventorys = GetInventorys();

	bool removed = false;

	for(auto i : Inventorys)
	{
		removed = i->RemoveItem(Obj);

		if(removed)
		{
			break;
		}
	}

	return  removed;	
}

void UInventoryManagerComponent::DropItem(UContainerObject* item)
{
	TArray<UInventoryComponent*> inventorys = GetInventorys();

	UInventoryComponent** invPtr = inventorys.FindByPredicate([&](UInventoryComponent* i)
    {
		return i->Items.Contains(item);
    });

	if(!invPtr)
	{
		return;
	}

	UInventoryComponent* inv = *invPtr;
	
	inv->DropItem(item);
}

